FROM debian:buster
RUN apt-get update
RUN apt-get install -y rustc cargo erlang elixir
RUN rm -rf /var/lib/apt/lists/*
RUN mix local.rebar --force
RUN mix local.hex --force
